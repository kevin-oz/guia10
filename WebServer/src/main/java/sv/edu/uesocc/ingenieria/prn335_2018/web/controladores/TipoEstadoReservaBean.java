/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoEstadoReservaFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin Figueroa
 */
@Named(value = "frmTipoEstadoReserva")
@ViewScoped
public class TipoEstadoReservaBean extends BackingBean<TipoEstadoReserva> implements Serializable {

    @EJB
    TipoEstadoReservaFacadeLocal tipoestadoreservaFacade_;
    TipoEstadoReserva tipoEstadoReserva_;

    private EstadosCRUD estado;

    public TipoEstadoReservaBean() {
    }

    /**
     * metodo de inicializacion,
     */
    @PostConstruct
    public void init() {
        estado = EstadosCRUD.NONE;
        tipoEstadoReserva_ = new TipoEstadoReserva();
        inicializar();
    }

    /*
    metodos sobrecargados
     */
    @Override
    protected MetodosGenericos<TipoEstadoReserva> getFacadeLocal() {
        return tipoestadoreservaFacade_;
    }

    @Override
    protected TipoEstadoReserva getEntity() {
        return tipoEstadoReserva_;
    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        tipoEstadoReserva_ = (TipoEstadoReserva) event.getObject();
    }

    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

//    @Override
//    protected Integer getRowD(TipoEstadoReserva object) {
//        return object.getIdTipoEstadoReserva();
//    }

    
    @Override
    protected TipoEstadoReserva getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (TipoEstadoReserva item : (List<TipoEstadoReserva>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdTipoEstadoReserva().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    
    @Override
    protected Object getKey(TipoEstadoReserva entity) {
        return entity.getIdTipoEstadoReserva();
    }

    /**
     * metodo para reiniciar campos
     */
    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        tipoEstadoReserva_ = new TipoEstadoReserva();
    }

    public void btnNuevoHandler(){
        estado = EstadosCRUD.NUEVO;
    }

    /*
 * Getters y Setters-----
     */
    public TipoEstadoReservaFacadeLocal getTipoestadoreservaFacade_() {
        return tipoestadoreservaFacade_;
    }

    public void setTipoestadoreservaFacade_(TipoEstadoReservaFacadeLocal tipoestadoreservaFacade_) {
        this.tipoestadoreservaFacade_ = tipoestadoreservaFacade_;
    }

    public TipoEstadoReserva getTipoEstadoReserva_() {
        return tipoEstadoReserva_;
    }

    public void setTipoEstadoReserva_(TipoEstadoReserva tipoEstadoReserva_) {
        this.tipoEstadoReserva_ = tipoEstadoReserva_;
    }

    public List<TipoEstadoReserva> getLista() {
        return lista;
    }

    public void setLista(List<TipoEstadoReserva> lista) {
        this.lista = lista;
    }

    public LazyDataModel<TipoEstadoReserva> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<TipoEstadoReserva> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public EstadosCRUD getEstado() {
        return estado;
    }

}
